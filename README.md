# Minecraft CustomNPC Skins

This projet is essentially meant to be used to store skins for the Minecraft CustomNPC Mod.<br>
A second project, [mc-customnpc-skins-updater](https://gitlab.com/naorimsenchai/mc-customnpc-skins-updater) was created in order to interact with this repository's JSON file.

## Prerequisites
- An IDE
- Python 3.10.9 or greater

## Installing
- Clone the project with `git clone https://gitlab.com/naorimsenchai/mc-customnpc-skins.git`
- Install the required libraries with `pip install <library_name>` if necessary

##  How to use
- Launch the **main.py** file by typing `python main.py` in the console located in the file's location
- The script will browse every single folder and generate a **skins.json** file containing a list of dictionaries featuring datas about all the skins it found
