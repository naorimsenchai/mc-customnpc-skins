import os
import json
import datetime

def current_time():
  now = datetime.datetime.now()
  return "[{:02d}:{:02d}:{:02d}]".format(now.hour, now.minute, now.second)

def check_extension(extension, allowed_extensions=(".bmp", ".png", ".jpg", ".jpeg", ".webp")):
  return extension in allowed_extensions

def check_path(path):
  return not path.startswith(".")

def list_files_to_json(root_dir=os.path.dirname(os.path.abspath(__file__)), json_file="skins.json"):
  success = 0
  warns = 0
  errors = 0
  
  if not os.path.exists(root_dir):
    raise ValueError(f"{root_dir} is not a valid directory.")
  if not os.access(root_dir, os.R_OK):
    raise PermissionError(f"Permission denied to read {root_dir}.")
  file_list = []
  for dirpath, dirnames, filenames in os.walk(root_dir):
    for filename in filenames:
      rel_path = os.path.relpath(dirpath, root_dir)
      if check_path(rel_path):
        file = os.path.splitext(filename)[0]
        extension = os.path.splitext(filename)[1]
        if check_extension(extension):
          file_list.append({'path': rel_path, 'file': file, 'extension': extension})
          if not (check_extension(extension, (".png"))):
            print(f'\033[93m{current_time()} Added "{os.path.join(rel_path, filename)}"... Consider making it a png next time\033[0m')
            warns += 1
          else:
            print(f'\033[94m{current_time()} Successfully added "{os.path.join(rel_path, filename)}"\033[0m')
            success += 1
        else:
          print(f'\033[91m{current_time()} File format not allowed for "{os.path.join(rel_path, filename)}"\033[0m')
          errors += 1
  try:
    os.remove(json_file)
  except FileNotFoundError:
    pass
  with open(json_file, 'w') as f:
    json.dump(file_list, f, indent=2)
      
  print(f'\n{current_time()} {success + warns}/{success + warns + errors} files added.')
  print(f'{current_time()} Finished with {warns} warning(s) and {errors} error(s).')

list_files_to_json()

os.system("pause")